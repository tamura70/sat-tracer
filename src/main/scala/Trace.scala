package web

case class TrailData(lit: Int, level: Int, reason: Seq[Int], learnt: Boolean)

class Trace {
  var trace: Seq[(String,Option[Seq[TrailData]])] = Seq.empty

  def addTrace(message: String): Unit =
    trace :+= (message, None)
  def addTrace(message: String, trail: Seq[TrailData]): Unit =
    trace :+= (message, Some(trail))

  def size = trace.size

  def getTrace(i: Int): Map[String,String] = trace(i) match {
    case (message, None) =>
      Map("message" -> message)
    case (message, Some(trail)) =>
      Map("message" -> message, "graph" -> getGraph(trail))
  }

  def getGraph(trail: Seq[TrailData]): String = {
    val learntColor = "red";
    val dg = new Digraph
    for (trailData <- trail) {
      val lit =
        if (trailData.lit == 0) "Conflict"
        else trailData.lit.toString
      val reason =
        if (trailData.reason.isEmpty) "null"
        else trailData.reason.mkString("", " ", "")
      if (trailData.learnt || (trailData.reason.size == 1 && trailData.reason.head == trailData.lit)) {
        dg.addNodeOpt(lit, Map("tooltip" -> reason, "color" -> learntColor))
      } else {
        dg.addNodeOpt(lit, Map("tooltip" -> reason))
      }
      dg.setLevel(lit, trailData.level)
      for (x <- trailData.reason.toSet - trailData.lit)
        dg.addArc((-x).toString, lit)
    }
    dg.toGraphviz()
  }
}
