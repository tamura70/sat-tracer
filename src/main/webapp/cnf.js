function cnfUNSAT(n) {
    if (n == 0)
        return [[]];
    let cnf = [];
    for (let clause of cnfUNSAT(n - 1)) {
        cnf.push(clause.concat(-n));
        cnf.push(clause.concat(n));
    }
    return cnf;
}

function range(from, to, by) {
    let r = [];
    if (by < 0) {
        for (let i = from; i > to; i += by)
            r.push(i);
    } else {
        for (let i = from; i < to; i += by)
            r.push(i);
    }
    return r;
}

function cnfWaerden(j, k, n) {
    let cnf = [];
    for (let d = 1; d <= n; d++) {
        for (let i = 1; i <= n; i++) {
            if (i+(j-1)*d <= n) {
                let clause = range(i, i+(j-1)*d+1, d);
                cnf.push(clause);
            }
        }
    }
    for (let d = 1; d <= n; d++) {
        for (let i = 1; i <= n; i++) {
            if (i+(k-1)*d <= n) {
                let clause = range(i, i+(k-1)*d+1, d).map(lit => -lit);
                cnf.push(clause)
            }
        }
    }
    return cnf;
}

function cnfPigeonHole(n) {
    function p(i, j) { return  i*n + j + 1; };
    let cnf = [];
    for (let i = 0; i <= n; i++)
        cnf.push(range(0, n, 1).map(j => p(i,j)));
    for (let j = 0; j < n; j++) {
        for (let i = 0; i <= n; i++)
            for (let k = i+1; k <= n; k++)
                cnf.push([-p(i,j), -p(k,j)]);
    }
    return cnf;
}
